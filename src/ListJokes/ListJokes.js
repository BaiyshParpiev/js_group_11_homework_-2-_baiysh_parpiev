import React, {Component} from 'react';

class ListJokes extends Component {
    shouldComponentUpdate(nextProps,) {
        return nextProps.label !== this.props.label || nextProps.id !== this.props.id
    }
    render() {
        return (
           <p key={this.props.id}>{this.props.label}</p>
        );
    }
}

export default ListJokes;