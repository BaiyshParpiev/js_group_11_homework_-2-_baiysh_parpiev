import React, {Component} from 'react';
import './Button.css'

class Button extends Component {
    render() {
        return (
           <button disabled={!this.props.disable} className={this.props.type} onClick={this.props.click}>{this.props.type}</button>
        );
    }
}

export default Button;