import React, {Component} from 'react';
import Button from "../Button/Button";
import InputNumber from "../InputNumber/InputNumber";
import ListJokes from "../ListJokes/ListJokes";
import './Builder.css'

const url = 'https://api.chucknorris.io/jokes/random';
const getJokes = async (count) => {
    const arrayJokes = [];
    for (let i = 0; i < count; i++) {
        const joke = await fetch(url)
        const response = await joke.json();
        arrayJokes.push({id: response.id, label: response.value})
    }
    return arrayJokes
}

class Builder extends Component {
    state = {
        inputNum: '',
        list: [],
        disable: true,
    }

    onHandlerChange = e => {
        this.setState({inputNum: parseInt(e.target.value)})
    }

    show = () => {
        if (isNaN(this.state.inputNum)) {
            alert('Enter Number');
            return;
        }
        const getValue = Promise.all([getJokes(this.state.inputNum)])
        getValue.then(value => {
            console.log(value)
            this.setState(prev => ({
                ...prev,
                list: [...this.state.list, ...value[0]],
                inputNum: '',
                disable: false
            }))
        });
    }

    new = () => {
        const getValue = Promise.all([getJokes(5)])
        getValue.then(value => {
            console.log(value)
            this.setState(prev => ({
                ...prev,
                list: [...this.state.list, ...value[0]],
                inputNum: '',
            }))
        });
    }

    render() {
        return (
            <div>
                <InputNumber onChange={e => this.onHandlerChange(e)} value={this.state.inputNum}/>
                <Button disable={this.state.disable} type="Show" click={this.show}/>
                <Button disable={!this.state.disable} type="New-jokes" click={this.new}/>
                <div className="list">
                    {this.state.list.map(joke => (
                        <ListJokes key={joke.id} id={joke.id} label={joke.label}/>
                    ))}
                </div>
            </div>
        );
    }
}

export default Builder;