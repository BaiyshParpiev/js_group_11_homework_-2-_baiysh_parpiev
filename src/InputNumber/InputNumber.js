import React, {Component} from 'react';

class InputNumber extends Component {
    render() {
        return (
            <div>
                <label>
                    Push Number
                    <input type="text" onChange={this.props.onChange}  value={this.props.value}/>
                </label>
            </div>
        );
    }
}

export default InputNumber;